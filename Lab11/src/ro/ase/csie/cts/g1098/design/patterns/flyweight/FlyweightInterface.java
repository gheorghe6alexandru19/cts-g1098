package ro.ase.csie.cts.g1098.design.patterns.flyweight;

import ro.ase.csie.cts.g1098.design.patterns.flyweight.initial.DisplaySettings;

public interface FlyweightInterface {

    public abstract void display(DisplaySettings displaySettings);
}
