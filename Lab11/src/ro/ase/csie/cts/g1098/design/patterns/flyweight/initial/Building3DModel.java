package ro.ase.csie.cts.g1098.design.patterns.flyweight.initial;

public class Building3DModel {

    int[] points;
    String fileName;

    DisplaySettings displaySettings;

    public Building3DModel(String fileName, DisplaySettings displaySettings) {
        this.fileName = fileName;
        this.displaySettings = displaySettings;
        System.out.println("Loading 3D model...");
        this.points = new int[1000000];
    }
}
