package ro.ase.csie.cts.g1098.design.patterns.flyweight;

import ro.ase.csie.cts.g1098.design.patterns.flyweight.initial.DisplaySettings;

public class Model3D implements FlyweightInterface{

    String fileName;
    int[] points;

    public Model3D(String fileName) {
        this.fileName = fileName;
        System.out.println("Loading model from " + this.fileName);
        this.points = new int [(int) 1e6];
    }

    @Override
    public void display(DisplaySettings displaySettings) {
        System.out.println("Generating the model based on the points array");
        System.out.println(String.format("Coordinates %d, %d", displaySettings.getX(), displaySettings.getY()));
        System.out.println("Building colour: " + displaySettings.getTextureColor());
    }
}
