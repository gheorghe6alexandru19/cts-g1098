package ro.ase.csie.cts.g1098.design.patterns.observer;

public class Main {
    public static void main(String[] args) {
        ConnectionModule connectionModule = new ConnectionModule();

        BackupModule backupModule = new
                BackupModule();

        connectionModule.register(backupModule);
        connectionModule.register(new NotificationsModule());

        connectionModule.openConnection();

        connectionModule.unregister(backupModule);
        connectionModule.openConnection();
    }
}
