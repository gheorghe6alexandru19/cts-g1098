1. For the initial commit, I placed the Account.java class in its entirety in the ro.ase.cts.g1098.assignment package
2. (Make sure the code formatting is applied) I noticed that the code had formatting issues ( poor indentation and spacing all over ), so I applied code formatting.
In IntelliJ, I did this by pressing Ctrl+Alt+Shift+L to bring up the "Reformat file menu" and automatically format the code.
3. (Use enums instead of int constants) I then saw that the code uses int constants to represent the account types, as well as "magic numbers" to calculate the total fee value.
I instead opted to use an enum under the name AccountType, which stores both
4. (In public classes, use accessor methods, not public fields) I made the fields of the Account class private, to encourage the use of accessor methods
5. (Use Intention-Revealing Names) I noticed that the name of some fields and functions were not that suggestive, so I renamed them to something more descriptive
This is also the moment I decided to make a separate Main class to test if the code works
6. (Always override toString) I figured toString would be useful for testing, so I renamed the to_string function to toString and tagged it as an override
7. (Do one Thing) The function that computes all the fees also has to choose the time period used in the calculation of the total fee.
We could instead outsource this choice to another class, which in this case, we could call something like LoanInterval.
This could also calculate the number of years the account has been opened for.
8. (Use Exceptions rather than Return codes) Inputting a value lower than 0 in the constructor or the setters currently throws a generic exception.
I created custom exceptions that extend the standard Exception to make it clearer
I also removed the print function as it was redundant, this could be considered part of the Do one Thing issue
9. If getMonthlyRate is mandatory, we can make it abstract by adding it to an interface, and making the classes implement said interface

I need a cup of coffee, sorry.