package ro.ase.cts.g1098.assignment.main;

import ro.ase.cts.g1098.assignment.models.LoanAccount;
import ro.ase.cts.g1098.assignment.models.AccountType;

public class Main {
    public static void main(String[] args) throws Exception {

        LoanAccount account = new LoanAccount(10000,0.05, AccountType.STANDARD);
        LoanAccount account2 = new LoanAccount(25000,1, AccountType.PREMIUM);

        //account.setLoanValue(-234);

        LoanAccount[] accounts = new LoanAccount[2];
        accounts[0] = account;
        accounts[1] = account2;

        System.out.println(account.getMonthlyRate());
        System.out.println(account);
        System.out.println(LoanAccount.computeAllAccountsFee(accounts));
    }
}
