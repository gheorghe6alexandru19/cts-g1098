package ro.ase.cts.g1098.assignment.models;

public enum AccountType {
    STANDARD( 0.0f), BUDGET( 0.0f), PREMIUM( 0.0125f), SUPER_PREMIUM(0.0125f);

    float fee;

    AccountType(float fee) {
        this.fee = fee;
    }

    public float getFee() {
        return fee;
    }
}
