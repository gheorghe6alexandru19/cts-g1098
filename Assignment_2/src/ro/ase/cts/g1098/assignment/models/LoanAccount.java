package ro.ase.cts.g1098.assignment.models;

import ro.ase.cts.g1098.assignment.exceptions.NegativeValueException;
import ro.ase.cts.g1098.assignment.interfaces.MonthlyRateInterface;

public class LoanAccount implements MonthlyRateInterface {
    private double loanValue;
    private double loanRate;
    private int daysActive;
    private AccountType accountType;

    LoanInterval loanInterval = new LoanInterval();

    public LoanAccount(double value, double rate, AccountType accountType) throws NegativeValueException {
        if (value < 0)
            throw new NegativeValueException();
        loanValue = value;
        this.loanRate = rate;
        this.accountType = accountType;
    }

    public double getLoanValue() {
        System.out.println("The loan value is " + this.loanValue);
        return this.loanValue;
    }

    public double getLoanRate() {
        System.out.println("The rate is " + loanRate);
        return this.loanRate;
    }

    //must have method - the lead has requested it in all classes
    @Override
    public double getMonthlyRate() {
        return (this.loanValue * this.loanRate);
    }

    public void setLoanValue(double value) throws NegativeValueException {
        if (value < 0)
            throw new NegativeValueException();
            this.loanValue = value;
    }

    @Override
    public String toString() {
        return "Loan: " + this.loanValue + "; rate: " + this.loanRate + "; days active: " + this.daysActive + "; Type: " + this.accountType + ";";
    }

    public static double computeAllAccountsFee(LoanAccount[] accounts) {
        double totalFee = 0.0;
        LoanAccount account;
        for (int i = 0; i < accounts.length; i++) {
            account = accounts[i];
            totalFee += account.accountType.getFee() * (account.loanValue * Math.pow(account.loanRate, account.loanInterval.getYearsActive(account.daysActive) ) - account.loanValue);   // interest-principal
        }
        return totalFee;
    }


}
