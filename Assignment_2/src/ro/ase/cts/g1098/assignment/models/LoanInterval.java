package ro.ase.cts.g1098.assignment.models;

public class LoanInterval {

    static final int YEARLY_LOAN_INTERVAL = 365;

    public int getYearsActive(int daysActive) {
        return daysActive / YEARLY_LOAN_INTERVAL;
    }
}
