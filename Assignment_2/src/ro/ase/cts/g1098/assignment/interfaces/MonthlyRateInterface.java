package ro.ase.cts.g1098.assignment.interfaces;

public interface MonthlyRateInterface {
    public abstract double getMonthlyRate();
}
