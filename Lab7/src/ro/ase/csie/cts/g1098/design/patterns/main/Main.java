package ro.ase.csie.cts.g1098.design.patterns.main;

import ro.ase.csie.cts.g1098.design.patterns.models.SuperHero;
import ro.ase.csie.cts.g1098.design.patterns.models.SuperPower;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        //SuperHero superman = new SuperHero();

        SuperHero superman = new SuperHero.SuperHeroBuilder("Pepsiman","superman.3d")
                .setPositive()
                .setLifePoints(1000)
                .setSuperPower(SuperPower.LASER_EYES)
                .build();

        SuperHero superman2 = (SuperHero) superman.clone();
    }
}
