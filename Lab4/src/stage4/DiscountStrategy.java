package stage4;

// stage 4
// - use the received productType and not the generic enum symbol
// - remove the switch because all branches do the same stuff (except NEW)

public class DiscountStrategy {

    public static final int PREMIUM_ACCOUNT_MIN_AGE = 10;
    public static final float PREMIUM_DISCOUNT = 0.15f;

    public float getFinalPrice(ProductType productType, float productPrice, int accountAgeInYears)
    {
        float finalPrice = 0;
        float accountDiscount = 0;

        if(productType != ProductType.NEW) {
            accountDiscount = (accountAgeInYears > PREMIUM_ACCOUNT_MIN_AGE) ? PREMIUM_DISCOUNT : (float)accountAgeInYears/100;
        }

        finalPrice = (productPrice - (productType.getDiscount() * productPrice)) - accountDiscount * (productPrice - (productType.getDiscount() * productPrice));

        return finalPrice;
    }
}