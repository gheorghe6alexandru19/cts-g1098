package stage6.models.marketing;

import stage6.interfaces.MarketingStrategyInterface;

public class SummerAccountMarketingStrategy implements MarketingStrategyInterface {
    @Override
    public float getAccountDiscount(int accountAgeInYears) {
        return 0.2f;
    }
}
