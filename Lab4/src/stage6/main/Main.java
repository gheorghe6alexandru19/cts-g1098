package stage6.main;

import stage6.interfaces.MarketingStrategyInterface;
import stage6.models.marketing.AccountMarketingStrategy;
import stage6.models.marketing.SummerAccountMarketingStrategy;
import stage6.models.product.Product;
import stage6.models.product.ProductType;

public class Main {
    public static void main(String[] args) {

        Product product = new Product();
        MarketingStrategyInterface marketingStrategy = new AccountMarketingStrategy();

        product.setAccountDiscountStrategy(marketingStrategy);

        float finalPrice = product.computeFinalPrice(ProductType.NORMAL, 1000, 10);
        System.out.println("Final price is " + finalPrice);

        product.setAccountDiscountStrategy(new SummerAccountMarketingStrategy());

        finalPrice = product.computeFinalPrice(ProductType.NORMAL, 1000, 10);
        System.out.println("Final summer price is " + finalPrice);


    }
}
