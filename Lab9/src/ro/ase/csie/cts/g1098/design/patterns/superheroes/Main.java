package ro.ase.csie.cts.g1098.design.patterns.superheroes;

import ro.ase.csie.cts.g1098.design.patterns.superheroes.disney.DisneyHero;
import ro.ase.csie.cts.g1098.design.patterns.superheroes.disney.DisneyToACMEAdapter;
import ro.ase.csie.cts.g1098.design.patterns.superheroes.disney.IDisneyHero;

public class Main {
    public static void main(String[] args) {

        IACMESuperHero superMan = new SuperHero("Superman",100);
        superMan.run(500);

        IDisneyHero vader = new DisneyHero("Vader");

        IACMESuperHero vaderHero = new DisneyToACMEAdapter(vader);

        vaderHero.heal(100);
        vaderHero.run(300);

        IACMESuperHero luke = new DisneyToACMEAdapter(new DisneyHero("Luke"));
    }
}
