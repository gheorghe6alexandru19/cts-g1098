package ro.ase.csie.cts.g1098.design.patterns.weapons;

public class Main {
    public static void main(String[] args) {
        //using weapons without the factory
        IWeapon weapon = new Pistol("Water pistol",true);
        //IWeapon secondWeapon = new MachineGun("Ultimate PKM", 100);

        IWeapon anotherWeapon = WeaponsFactory.getWeapon(WeaponType.BAZOOKA,"St. Javelin");
        ((Bazooka) anotherWeapon).setMaxDistance(2000);

    }
}
