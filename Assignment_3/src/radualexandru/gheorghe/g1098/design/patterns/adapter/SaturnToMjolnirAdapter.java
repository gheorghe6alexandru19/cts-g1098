package radualexandru.gheorghe.g1098.design.patterns.adapter;

public class SaturnToMjolnirAdapter implements MjolnirProductInterface{
    SaturnProduct saturn;

    public SaturnToMjolnirAdapter(SaturnProduct saturn) {
        this.saturn = saturn;
    }


    @Override
    public String getName() {
        return saturn.getName();
    }

    @Override
    public Long getProductionDate() {
        return (saturn.getExpirationDate() - saturn.getExpirationTimeInMillis());
    }

    @Override
    public int getExpirationDays() {
        return (int) (saturn.getExpirationTimeInMillis()/1000/60/24);
    }

    @Override
    public void checkExpirationOnDate(long date) {
        if(saturn.getExpirationDate() < System.currentTimeMillis()) {
            System.out.println("The product is expired");
        } else System.out.println("The product is still usable");
    }
}
