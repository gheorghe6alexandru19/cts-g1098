package radualexandru.gheorghe.g1098.design.patterns.adapter;

public class SaturnRing extends SaturnProduct{

    public SaturnRing(String name, Long expirationDate, Long expirationTimeInMillis) {
        super(name, expirationDate, expirationTimeInMillis);
    }
}
