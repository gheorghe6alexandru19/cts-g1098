BACHELOR THEIS STOPIC:
Managing a supply chain using Blockchain technology

Problem:
Mjolnir, your parent company, has recently performed a merger with another company called Saturn.
Saturn also works in the domain of supply chain management, but their product interface differs slightly from yours, although it serves the same function.
Find a way to make use of the Saturn interface within Mjolnir's systems via a design pattern.

An **adapter** would be the ideal solution for this problem.

Pros:
    - flexible
    - solves incompatibility issues between similar, yet incompatible classes
Cons:
    - may be easier to refactor instead, depending on how large the collections are
