package radualexandru.gheorghe.g1098.design.patterns.adapter;

public abstract class SaturnProduct {
    private String name;
    private Long expirationDate;
    private Long expirationTimeInMillis;

    public SaturnProduct(String name, Long expirationDate, Long expirationTimeInMillis) {
        this.name = name;
        this.expirationDate = expirationDate;
        this.expirationTimeInMillis = expirationTimeInMillis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getExpirationTimeInMillis() {
        return expirationTimeInMillis;
    }

    public void setExpirationTimeInMillis(Long expirationTimeInMillis) {
        this.expirationTimeInMillis = expirationTimeInMillis;
    }

    public void checkExpired() {
        if(expirationTimeInMillis < System.currentTimeMillis()) {
            System.out.println("Expired");
        } else System.out.println("Not expired");
    }
}
