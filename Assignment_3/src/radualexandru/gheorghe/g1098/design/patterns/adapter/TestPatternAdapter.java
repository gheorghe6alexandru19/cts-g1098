package radualexandru.gheorghe.g1098.design.patterns.adapter;

public class TestPatternAdapter {
    public static void main(String[] args) {
        System.out.println("-- ADAPTER TEST --");

        MjolnirProduct mjolnirProduct = new MjolnirProduct("Lettuce",System.currentTimeMillis(),30);
        mjolnirProduct.checkExpirationOnDate(System.currentTimeMillis() + 31 * 24 * 60 * 1000);

        SaturnProduct saturnProduct = new SaturnRing("Cabbage", System.currentTimeMillis() + (30 * 24 * 60 * 1000), (long) (30 * 24 * 60 * 1000));
        saturnProduct.checkExpired();

        SaturnToMjolnirAdapter saturnAdapter = new SaturnToMjolnirAdapter(saturnProduct);
        saturnAdapter.checkExpirationOnDate(System.currentTimeMillis());

    }
}
