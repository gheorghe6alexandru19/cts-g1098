package radualexandru.gheorghe.g1098.design.patterns.adapter;

public class MjolnirProduct implements MjolnirProductInterface{
    private final String name;
    private final Long productionDate;
    private final int expirationDays;

    public MjolnirProduct(String name, Long productionDate, int expirationDays) {
        this.name = name;
        this.productionDate = productionDate;
        this.expirationDays = expirationDays;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getProductionDate() {
        return productionDate;
    }

    @Override
    public int getExpirationDays() {
        return expirationDays;
    }

    @Override
    public void checkExpirationOnDate(long date) {
        if(this.productionDate + (long) this.expirationDays *24*60*1000 < date) {
            System.out.println("The product is expired");
        } else System.out.println("The product is still usable");
    }
}
