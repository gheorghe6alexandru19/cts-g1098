package radualexandru.gheorghe.g1098.design.patterns.adapter;

public interface MjolnirProductInterface {

    String getName();
    Long getProductionDate();
    int getExpirationDays();

    void checkExpirationOnDate(long date);
}
