package radualexandru.gheorghe.g1098.design.patterns.proxy;

public class CodeAuthProxy implements CodeAuthInterface{

    CodeAuthInterface codeAuth;

    int attempts = 0;

    public CodeAuthProxy(CodeAuthInterface codeAuth) {
        this.codeAuth = codeAuth;
    }

    @Override
    public boolean access(Long code, String token) {

        if(attempts >= 3) {
            System.out.println("Unable to establish link to node details.");
            return false;
        }
        boolean result = this.codeAuth.access(code,token);
        if(!result) {
            attempts++;
            System.out.println("Wrong code, used " + attempts + "/3 attempts.");
        }
        else attempts = 0;
        return result;
    }
}
