package radualexandru.gheorghe.g1098.design.patterns.proxy;

public class TestPatternProxy {
    public static void main(String[] args) {
        System.out.println("-- PROXY TEST --");
        CodeAuth codeAuth = new CodeAuth(130756L,"a354fgvd234kmllf2");

        Long[] authTokens = new Long[] {324519L, 453293L, 234212L, 130756L};
        CodeAuthProxy proxy = new CodeAuthProxy(codeAuth);

        for (Long authToken: authTokens) {
            if(proxy.access(authToken,"a354fgvd234kmllf2")) {
                System.out.println("Auth token located");
            }
        }
    }
}
