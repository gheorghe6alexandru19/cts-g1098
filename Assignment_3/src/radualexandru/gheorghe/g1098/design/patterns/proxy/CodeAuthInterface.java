package radualexandru.gheorghe.g1098.design.patterns.proxy;

public interface CodeAuthInterface {
    boolean access(Long code, String token);
}
