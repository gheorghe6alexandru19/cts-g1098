BACHELOR THEIS STOPIC:
Managing a supply chain using Blockchain technology

Problem:
Your company has decided to provide an authentication system application for clients using your system to track their products.
This allows them to mark products as tampered or to change the delivery date.
A client reported security concerns due to the two-factor authentication system being easy to brute force.
Find a way to improve security without interrupting the login system

The **proxy** pattern would be a good choice in this case.

Pros:
    - lets us control access to an object
    - relatively easy to implement
Cons:
    -
