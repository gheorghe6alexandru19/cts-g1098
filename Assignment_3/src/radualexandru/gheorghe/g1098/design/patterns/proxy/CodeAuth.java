package radualexandru.gheorghe.g1098.design.patterns.proxy;

public class CodeAuth implements CodeAuthInterface{

    Long code;
    String token;

    public CodeAuth(Long code, String token) {
        this.code = code;
        this.token = token;
    }

    @Override
    public boolean access(Long code, String token) {
        return code.equals(this.code) && token.equals(this.token);
    }
}
