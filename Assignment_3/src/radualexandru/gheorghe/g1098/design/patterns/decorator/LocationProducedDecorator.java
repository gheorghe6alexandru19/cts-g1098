package radualexandru.gheorghe.g1098.design.patterns.decorator;

public class LocationProducedDecorator extends  ProductDetailsDecorator{

    Float coordinateX;
    Float coordinateY;

    public LocationProducedDecorator(ProductDetailsInterface productDetails, Float coordinateX, Float coordinateY) {
        super(productDetails);
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    @Override
    public void productTampered() {
        System.out.println("Product location matches description, product is OK");
    }
}
