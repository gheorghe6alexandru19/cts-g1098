package radualexandru.gheorghe.g1098.design.patterns.decorator;

public abstract class ProductDetailsDecorator implements ProductDetailsInterface{

    ProductDetailsInterface productDetails;

    public ProductDetailsDecorator(ProductDetailsInterface productDetails) {
        this.productDetails = productDetails;
    }

    @Override
    public String getName() {
        return productDetails.getName();
    }

    @Override
    public Long getPackagingDate() {
        return productDetails.getPackagingDate();
    }

    @Override
    public int getExpirationLengthInDays() {
        return productDetails.getExpirationLengthInDays();
    }

    @Override
    public void productTampered() {
        productDetails.productTampered();
    }

    @Override
    public void productExpired() {
        productDetails.productExpired();
    }
}
