package radualexandru.gheorghe.g1098.design.patterns.decorator;

public class TestPatternDecorator {
    public static void main(String[] args) {
        System.out.println("-- DECORATOR TEST --");
        ProductDetailsInterface milk = new ProductDetails("Pasteurized Milk",System.currentTimeMillis(),20);

        milk.productTampered();

        LocationProducedDecorator milkGeolocated = new LocationProducedDecorator(milk,46.785619333110404F, 23.536814799998584F);
        milkGeolocated.productTampered();
    }

}
