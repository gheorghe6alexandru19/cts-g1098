BACHELOR THEIS STOPIC:
Managing a supply chain using Blockchain technology

Problem:
Your company has decided that some of the products should feature product tampering checks based on geolocation information.
This measure was requested due to attempted counterfeiting in batches of milk en route to Kaufland.
You want to do this without modifying the base class for products.

The **decorator** design pattern may be an ideal solution, as it allows us to dynamically extend class functionality.

Pros:
    - flexible
    - can extend a class without using inheritance
Cons:
    - complex to write
    - may be hard to maintain in the future
