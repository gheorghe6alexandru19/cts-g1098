package radualexandru.gheorghe.g1098.design.patterns.decorator;

public interface ProductDetailsInterface {
    String getName();
    Long getPackagingDate();
    int getExpirationLengthInDays();

    void productTampered();
    void productExpired();
}
