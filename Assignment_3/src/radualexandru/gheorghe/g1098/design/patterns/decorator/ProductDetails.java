package radualexandru.gheorghe.g1098.design.patterns.decorator;

public class ProductDetails implements ProductDetailsInterface{

    String name;
    Long packagingDate;
    int expirationLengthInDays;

    public ProductDetails(String name, Long packagingDate, int expirationLengthInDays) {
        this.name = name;
        this.packagingDate = packagingDate;
        this.expirationLengthInDays = expirationLengthInDays;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getPackagingDate() {
        return packagingDate;
    }

    @Override
    public int getExpirationLengthInDays() {
        return expirationLengthInDays;
    }

    @Override
    public void productTampered() {
        System.out.println("Product may be tampered");
    }

    @Override
    public void productExpired() {

    }


}
