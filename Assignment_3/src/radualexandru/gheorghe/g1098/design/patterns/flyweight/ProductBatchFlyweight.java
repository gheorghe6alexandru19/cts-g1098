package radualexandru.gheorghe.g1098.design.patterns.flyweight;

import java.util.ArrayList;
import java.util.Random;

public class ProductBatchFlyweight implements ProductBatchGeolocInterface{

    String csvDetails;
    Long batchID;
    ArrayList<Integer> productIDs;

    public ProductBatchFlyweight(String csvDetails, Long batchID) {
        this.csvDetails = csvDetails;
        this.batchID = batchID;

        productIDs = new ArrayList<>();

        System.out.println("Loading batch " + csvDetails);

        Random random = new Random();

        for(int i = 0; i < 100; i++) {
            productIDs.add(random.nextInt(10000));
        }
        System.out.println("Loaded");
    }

    @Override
    public void showCoordinates(ProductBatch batch) {
        System.out.println(batch.productBatchName + "location: " + batch.coordsX + ", " + batch.coordsY);
    }
}
