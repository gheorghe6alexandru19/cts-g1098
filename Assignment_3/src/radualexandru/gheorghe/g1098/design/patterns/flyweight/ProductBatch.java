package radualexandru.gheorghe.g1098.design.patterns.flyweight;

public class ProductBatch {

    float coordsX;
    float coordsY;
    String productBatchName;

    public ProductBatch(float coordsX, float coordsY, String productBatchName) {
        this.coordsX = coordsX;
        this.coordsY = coordsY;
        this.productBatchName = productBatchName;
    }
}
