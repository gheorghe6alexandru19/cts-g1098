package radualexandru.gheorghe.g1098.design.patterns.flyweight;

public interface ProductBatchGeolocInterface {
    void showCoordinates(ProductBatch batch);
}
