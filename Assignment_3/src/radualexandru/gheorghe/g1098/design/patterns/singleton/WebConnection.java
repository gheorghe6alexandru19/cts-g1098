package radualexandru.gheorghe.g1098.design.patterns.singleton;

public class WebConnection {
    private static WebConnection connection;

    String connectionProtocol;
    String IPAddress = "123.123.123.123";
    int port = 8000;
    String username;
    String password;

    private WebConnection() {

    }

    private WebConnection(String connectionProtocol, String IPAddress, int port, String username, String password) {
        this.connectionProtocol = connectionProtocol;
        this.IPAddress = IPAddress;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    public static synchronized WebConnection getInstance() {
        if(connection == null) {
            connection = new WebConnection();
        }
        return connection;
    }
}
