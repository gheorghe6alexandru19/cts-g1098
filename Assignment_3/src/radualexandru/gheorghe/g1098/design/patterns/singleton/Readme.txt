BACHELOR THEIS STOPIC:
Managing a supply chain using Blockchain technology

Problem:
In order to display data about a product, your application scans a QR code that opens a link displaying info about a blockchain node.
All nodes use the same website for display, and must be connected using the same set of credentials.

The company decides to use a **singleton** pattern to solve this issue
Pros:
    - Fits the use case, our credential class needs only one instance and a global access point
    - Could be inherited from or extended in the near future, should the need arise

Cons:
    - Can hinder unit testing because singletons are usually implemented statically
    - Must ensure that the singleton is thread-safe
