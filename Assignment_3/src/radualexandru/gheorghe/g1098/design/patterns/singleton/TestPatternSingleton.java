package radualexandru.gheorghe.g1098.design.patterns.singleton;

public class TestPatternSingleton {
    public static void main(String[] args) {
        WebConnection connection = WebConnection.getInstance();

        System.out.println("-- SINGLETON TEST --");
        System.out.println("Connecting to: " + connection.IPAddress + ":" + connection.port);
        System.out.println("Success");
    }
}
