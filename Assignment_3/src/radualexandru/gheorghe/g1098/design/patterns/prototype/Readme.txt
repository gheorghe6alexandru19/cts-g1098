BACHELOR THEIS STOPIC:
Managing a supply chain using Blockchain technology

Problem:
Your blockchain-powered site allows you to view product batches, which are formed by adding multiple products together.
Should the batches get large, the computation time may possibly increase to unacceptable levels.
Find a way to reduce the time needed to display a batch.

When time is an issue, the **prototype** pattern is ideal.

Pros:
    - easy to make, only requires you to override the clone() method correctly
    - favourable where time is an issue
Cons:
    - consumes a lot of memory
