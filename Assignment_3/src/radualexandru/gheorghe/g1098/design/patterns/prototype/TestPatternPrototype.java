package radualexandru.gheorghe.g1098.design.patterns.prototype;

public class TestPatternPrototype {
    public static void main(String[] args) {
        System.out.println("-- PROTOTYPE TEST --");
        ProductBatch lettuceBatch = new ProductBatch("Greens Inc.","lettuceBatch.csv");

        ProductBatch lettuceBatch2 = (ProductBatch) lettuceBatch.clone();
        lettuceBatch2.setBatchProducer("Romanian Foods SRL");

        System.out.println(lettuceBatch);
        System.out.println(lettuceBatch2);


    }
}
