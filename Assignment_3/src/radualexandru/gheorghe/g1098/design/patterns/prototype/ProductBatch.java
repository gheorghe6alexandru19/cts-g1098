package radualexandru.gheorghe.g1098.design.patterns.prototype;

import java.util.ArrayList;
import java.util.Random;

public class ProductBatch implements Cloneable{

    String batchProducer;
    String batchDocument;

    ArrayList<Integer> productIDs;

    public ProductBatch(String batchProducer, String batchDocument) {
        this.batchProducer = batchProducer;
        this.batchDocument = batchDocument;

        productIDs = new ArrayList<>();

        System.out.println("Loading batch " + batchProducer);

        Random random = new Random();

        for(int i = 0; i < 100; i++) {
            productIDs.add(random.nextInt(10000));
        }
        System.out.println("Loaded");
    }

    private ProductBatch() {
        batchProducer = "";
        batchDocument = "";
        productIDs = null;
    }

    public String getBatchProducer() {
        return batchProducer;
    }

    public void setBatchProducer(String batchProducer) {
        this.batchProducer = batchProducer;
    }

    public String getBatchDocument() {
        return batchDocument;
    }

    public void setBatchDocument(String batchDocument) {
        this.batchDocument = batchDocument;
    }

    @Override
    public Object clone(){
        ProductBatch cpy = new ProductBatch();
        cpy.batchProducer = this.batchProducer;
        cpy.batchDocument = this.batchDocument;
        cpy.productIDs = (ArrayList<Integer>) this.productIDs.clone();

        return cpy;
    }

    @Override
    public String toString() {
        return "ProductBatch{" +
                "batchProducer='" + batchProducer + '\'' +
                ", batchDocument='" + batchDocument + '\'' +
                ", productIDs=" + productIDs +
                '}';
    }
}
