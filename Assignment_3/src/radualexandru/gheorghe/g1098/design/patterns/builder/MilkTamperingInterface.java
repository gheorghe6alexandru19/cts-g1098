package radualexandru.gheorghe.g1098.design.patterns.builder;

public interface MilkTamperingInterface {
    void reportTampering();
}
