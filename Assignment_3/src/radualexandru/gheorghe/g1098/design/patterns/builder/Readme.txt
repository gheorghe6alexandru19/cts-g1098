BACHELOR THEIS STOPIC:
Managing a supply chain using Blockchain technology

Problem:
You need a way to create multiple milk products with different details in order to speed up node creation on the blockchain.

The **builder** design pattern would be a good solution for this problem.

Pros:
    - if coded correctly, makes it very easy to use when creating distinct objects
    - object code will be encapsulated, save for the builder itself
Cons:
    - long to code
