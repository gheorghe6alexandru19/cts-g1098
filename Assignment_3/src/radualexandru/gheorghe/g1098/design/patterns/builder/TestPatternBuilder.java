package radualexandru.gheorghe.g1098.design.patterns.builder;

public class TestPatternBuilder {
    public static void main(String[] args) {
        System.out.println("-- BUILDER TEST --");

        MilkProduct milk = new MilkProduct.MilkProductBuilder("Ultrapasteurized Milk","Pilos")
                .setContainerVolumeInMl(2000f)
                .setFatContent(0.015f)
                .markAsPasteurized()
                .build();

        System.out.println(milk);
    }
}
