package radualexandru.gheorghe.g1098.design.patterns.builder;

public class MilkProduct {

    String name;
    String producer;
    float fatContent;
    float containerVolumeInMl;

    boolean pasteurized;

    MilkTamperingInterface milkTamperingInterface;

    private void setName(String name) {
        this.name = name;
    }

    private void setProducer(String producer) {
        this.producer = producer;
    }

    private void setFatContent(float fatContent) {
        this.fatContent = fatContent;
    }

    private void setContainerVolumeInMl(float containerVolumeInMl) {
        this.containerVolumeInMl = containerVolumeInMl;
    }

    private void setPasteurized(boolean pasteurized) {
        this.pasteurized = pasteurized;
    }

    private void setMilkTamperingInterface(MilkTamperingInterface milkTamperingInterface) {
        this.milkTamperingInterface = milkTamperingInterface;
    }

    private MilkProduct() {

    }

    @Override
    public String toString() {
        return "MilkProduct{" +
                "name='" + name + '\'' +
                ", producer='" + producer + '\'' +
                ", fatContent=" + fatContent +
                ", containerVolumeInMl=" + containerVolumeInMl +
                ", pasteurized=" + pasteurized +
                ", milkTamperingInterface=" + milkTamperingInterface +
                '}';
    }

    public static class MilkProductBuilder{
        private final MilkProduct milkProduct;

        public MilkProductBuilder(String name, String producer) {
            this.milkProduct = new MilkProduct();
            this.milkProduct.name = name;
            this.milkProduct.producer = producer;
        }

        public MilkProductBuilder markAsPasteurized() {
            this.milkProduct.pasteurized = true;
            return this;
        }

        public MilkProductBuilder setName(String name) {
            this.milkProduct.setName(name);
            return this;
        }

        public MilkProductBuilder setProducer(String producer) {
            this.milkProduct.setProducer(producer);
            return this;
        }

        public MilkProductBuilder setFatContent(float fatContent) {
            this.milkProduct.setFatContent(fatContent);
            return this;
        }

        public MilkProductBuilder setContainerVolumeInMl(float containerVolumeInMl) {
            this.milkProduct.setContainerVolumeInMl(containerVolumeInMl);
            return this;
        }

        public MilkProductBuilder setPasteurized(boolean pasteurized) {
            this.milkProduct.setPasteurized(pasteurized);
            return this;
        }

        public MilkProductBuilder setMilkTamperingInterface(MilkTamperingInterface milkTamperingInterface) {
            this.milkProduct.setMilkTamperingInterface(milkTamperingInterface);
            return this;
        }

        public MilkProduct build() {
            return this.milkProduct;
        }
    }
}
