package radualexandru.gheorghe.g1098.design.patterns.factory.simple.product;

public class ProductFactory {
    public static AbstractProduct createProduct(ProductType productType, String productName) {
        AbstractProduct product = null;

        switch(productType) {
            case DAIRY -> {
                product = new MilkProduct();
                product.setProductName(productName);
                product.setProductionDateLong(System.currentTimeMillis());
            }
            case VEGETABLE -> {
                product = new LettuceProduct();
                product.setProductName(productName);
                product.setProductionDateLong(System.currentTimeMillis());
            }
            default -> throw new UnsupportedOperationException();
        }

        return product;
    }

}
