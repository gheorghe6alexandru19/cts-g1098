package radualexandru.gheorghe.g1098.design.patterns.factory.simple.product;

public abstract class AbstractProduct {

    private String productName;
    private Long productionDateLong;
    private Long arrivalDateLong;

    public abstract String getProductType();

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductionDateLong(Long productionDateLong) {
        this.productionDateLong = productionDateLong;
    }

    public void setArrivalDateLong(Long arrivalDateLong) {
        this.arrivalDateLong = arrivalDateLong;
    }
}
