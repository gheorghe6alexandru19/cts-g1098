package radualexandru.gheorghe.g1098.design.patterns.factory.simple.product;

public enum ProductType {
    DAIRY,
    VEGETABLE
}
