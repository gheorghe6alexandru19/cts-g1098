package radualexandru.gheorghe.g1098.design.patterns.factory.simple.product;

public class MilkProduct extends AbstractProduct{

    private String barnName;
    private float fatContent;

    @Override
    public String getProductType() {
        return "Dairy";
    }

    public void setBarnName(String barnName) {
        this.barnName = barnName;
    }

    public void setFatContent(float fatContent) {
        this.fatContent = fatContent;
    }

    @Override
    public String toString() {
        return "MilkProduct{" +
                "barnName='" + barnName + '\'' +
                ", fatContent=" + fatContent +
                '}';
    }
}
