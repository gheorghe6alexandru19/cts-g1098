package radualexandru.gheorghe.g1098.design.patterns.factory.simple;

import radualexandru.gheorghe.g1098.design.patterns.factory.simple.product.MilkProduct;
import radualexandru.gheorghe.g1098.design.patterns.factory.simple.product.ProductFactory;
import radualexandru.gheorghe.g1098.design.patterns.factory.simple.product.ProductType;

public class TestPatternSimpleFactory {
    public static void main(String[] args) {
        System.out.println("-- SIMPLE FACTORY TEST --");
        MilkProduct milk = (MilkProduct) ProductFactory.createProduct(ProductType.DAIRY,"Milk");

        milk.setArrivalDateLong(System.currentTimeMillis() + 100000);
        milk.setBarnName("Napolact");
        milk.setFatContent(0.015f);

        System.out.println(milk);
    }
}
