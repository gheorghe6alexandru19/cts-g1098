BACHELOR THEIS STOPIC:
Managing a supply chain using Blockchain technology

Problem:
Your blockchain registers products based on certain characteristics. However, each product has certain characteristics.
For example, milk and lettuce - although they are both products, milk is not a vegetable, but lettuce is

The company suggests using a **simple factory** for this

Pros:
    - easy to expand
    - easy to test
Cons:
    - number of classes being worked with can get very large

