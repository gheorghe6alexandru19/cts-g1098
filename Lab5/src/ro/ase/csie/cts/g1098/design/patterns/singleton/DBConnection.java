package ro.ase.csie.cts.g1098.design.patterns.singleton;

public class DBConnection {
    private static DBConnection connection;

    String IPAddress;
    String userName;
    String userPass;

    private DBConnection() {

    }

    private DBConnection(String IPAddress, String userName, String userPass) {
        this.IPAddress = IPAddress;
        this.userName = userName;
        this.userPass = userPass;
    }

    public static synchronized DBConnection getInstance() {
        if(connection == null) {
            connection = new DBConnection();
        }
        return connection;
    }
}
